using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public abstract class Interaction : ScriptableObject
    {
        public abstract void Execute(PlayerLocomotion player, InteractionHook hook);
    }
}


