using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space
{
    public class HandleInteractions : StateAction
    {
        PlayerStatesManager states;
        
        public HandleInteractions(PlayerStatesManager states)
        {
            this.states = states;
        }
        public override bool Execute()
        {
            bool retVal  = false;
            if(states.hasInteraction)
            {
                if(states.isInteracting)
                {
                   // InteractionHook hook = states.GetInteractionHook;
                   // hook.ExecuteInteraction(states, states.deltaTime);
                }
            }
            return retVal;
        }
    }
}


