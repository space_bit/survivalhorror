using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace space2
{
    public class PlayerLocomotion : State
    {
        public bool tankControls = true;
        [Header("Inputs")]
        public bool aimingInput;
        public bool debugAnim;
        public bool fireInput;
        public bool interactInput;
        public bool sprintInput;
        float horizontal;
        float vertical;
        float moveAmount;
        Vector3 moveDirection;
        [Header("References")]
        public Transform cameraTransform;
        public NavMeshAgent agent;
        public Animator anim;
        public InventoryManager inventoryManager;
        public InteractionHook currentInteraction;
        [Header("States")]
        public bool isAiming;
        public bool isFiring;
        public bool isInteracting;
        public bool isSprinting;
        public bool isLocal = true;
        [Header("Stats")]
        public float rotationSpeed = 2;
        public float movementSpeed = 1;
        public float rotationSpeedCamOriented = 3;
        float lastFired;
        Vector3 scaleVector = new Vector3(1, 0, 1);
        Transform mTransform;

        private void Start()
        {
            inventoryManager.LoadWeapon(this, inventoryManager.startingWeapon);
        }

        public override void Tick(float delta)
        {
            if (mTransform == null)
            {
                mTransform = this.transform;
            }
            HandleInput();
            if (interactInput)
            {
                if (currentInteraction != null)
                {
                    if (!isInteracting)
                    {
                        isInteracting = true;
                        interactInput = false;
                        aimingInput = false;
                        fireInput = false;
                        sprintInput = false;
                    }

                }
            }
            if (isInteracting)
            {
                HandleInteraction(delta);
            }
            else
            {
                HandleNormalMovement(delta);
            }
        }

        void HandleInput()
        {
            aimingInput = Input.GetButton("Fire2");
            fireInput = Input.GetButton("Fire1");
            sprintInput = Input.GetButton("Fire3");
            interactInput = Input.GetButtonDown("Jump");
            horizontal = Input.GetAxis("Horizontal");
            vertical = Input.GetAxis("Vertical");
            moveAmount = Mathf.Clamp01(Mathf.Abs(horizontal) + Mathf.Abs(vertical));

            Vector3 camForward = Vector3.Scale(cameraTransform.forward, scaleVector).normalized;
            moveDirection = (vertical * camForward) + (horizontal * cameraTransform.right);
            moveDirection.Normalize();
            if (debugAnim)
                aimingInput = true;
        }
        void HandleNormalMovement(float delta)
        {
            if (isAiming)
            {
                if (!aimingInput)
                {
                    isAiming = false;
                    anim.CrossFade("Locomotion", 0.1f);
                }
                TankControls(delta);
                if (fireInput)
                {
                    HandleShooting();
                }
            }
            else
            {
                if (aimingInput)
                {
                    isAiming = true;
                    anim.CrossFade("Aiming", 0.1f);
                }
                if (tankControls)
                {
                    TankControls(delta);
                }
                else
                {
                    CameraOrientedControls(delta);
                }
            }
            if(sprintInput)
            {
                movementSpeed = 3f;
            }
            else
            {
                movementSpeed = 2f;
            }
        }
        public void TankControls(float delta)
        {
            float forwardAmount = vertical * movementSpeed;
            Vector3 forward = mTransform.forward * forwardAmount;// * delta;
            agent.velocity = forward;
            //mTransform.position += forward;

            float turnAmount = horizontal * rotationSpeed;
            Vector3 eulers = mTransform.eulerAngles;
            eulers.y += turnAmount * delta;
            mTransform.eulerAngles = eulers;

            float forwardAnim = (forwardAmount > 0) ? 1 : (forwardAmount < 0) ? -1 : 0;
            if (Mathf.Abs(turnAmount) > 0)
            {
                if (forwardAmount >= 0)
                {
                    forwardAnim = 1;
                }
                else
                {
                    forwardAmount = -1;
                }
            }
            anim.SetFloat("forward", forwardAnim, 0.2f, delta);
        }
        public void CameraOrientedControls(float delta)
        {
            Vector3 targetDirection = Vector3.zero;
            float forwardAnim = 0;
            if (moveAmount > 0.1f)
            {
                forwardAnim = 1;
                targetDirection = moveDirection;
                Quaternion targetRot = Quaternion.LookRotation(moveDirection);
                mTransform.rotation = Quaternion.Slerp(mTransform.rotation, targetRot, delta * rotationSpeedCamOriented);
            }
            float forwardAmount = moveAmount * movementSpeed;
            Vector3 forward = mTransform.forward * forwardAmount;// * delta;
            agent.velocity = forward;
            //mTransform.position += forward;
            anim.SetFloat("forward", forwardAnim, 0.2f, delta);
        }
        void HandleShooting()
        {
            Weapon cw = inventoryManager.GetCurrentWeapon();
            if (cw != null)
            {
                if (cw.fireRate < Time.realtimeSinceStartup - lastFired)
                {
                    lastFired = Time.realtimeSinceStartup;

                    Vector3 origin = mTransform.position + Vector3.up;
                    Vector3 direction = mTransform.forward;

                    Debug.DrawRay(origin, direction * 20);
                    RaycastHit hit;
                    if (Physics.Raycast(origin, direction, out hit, 20))
                    {
                        Debug.Log(hit.transform.name);
                        IShootable shootable = hit.transform.GetComponent<IShootable>();
                        if (shootable != null)
                        {
                            shootable.OnHit(cw);
                        }
                    }
                }
            }
        }

        #region 
        void HandleInteraction(float delta)
        {
            currentInteraction.ExecuteInteraction(this, delta);
        }
        public void LoadInteraction(InteractionHook interHook)
        {
            currentInteraction = interHook;
            ReferenceManager.singleton.interactionTextObj.SetActive(true);
            ReferenceManager.singleton.interactionText.text = interHook.interactionText;
        }
        void UnloadInteraction()
        {
            if (currentInteraction.isOneShot)
            {
                UnloadInteractionActual();
            }
            else
            {
                LoadInteraction(currentInteraction);
            }

        }

        public void UnloadInteractionActual()
        {
            currentInteraction = null;
            ReferenceManager.singleton.interactionTextObj.SetActive(false);
        }
        public void StopInteraction()
        {
            Time.timeScale = 1;
            isInteracting = false;
            UnloadInteraction();
        }
        #endregion
    }
}
