﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space
{
    public class PlayerMovement : StateAction
    {
        PlayerStatesManager playerStates;
        string aimingId;
        public PlayerMovement(PlayerStatesManager playerStatesManager, string aimingId)
        {
            this.aimingId = aimingId;
            playerStates = playerStatesManager;
        }
        public override bool Execute()
        {
            if(playerStates.isAiming)
            {
                playerStates.SetState(aimingId);
                return true;
            }
            if (playerStates.tankControls)
            {
                return TankControls();
            }
            else
            {
                return CameraOrientedControls();
            }
        }
        public bool TankControls()
        {
            float forwardAmount = playerStates.vertical * playerStates.movementSpeed;
            Vector3 forward = playerStates.mTransform.forward * forwardAmount;// * playerStates.deltaTime;
            playerStates.agent.velocity = forward;
            //playerStates.mTransform.position += forward;

            float turnAmount = playerStates.horizontal * playerStates.rotationSpeed;
            Vector3 eulers = playerStates.mTransform.eulerAngles;
            eulers.y += turnAmount * playerStates.deltaTime;
            playerStates.mTransform.eulerAngles = eulers;

            float forwardAnim = (forwardAmount > 0) ? 1 : (forwardAmount < 0)? -1 : 0;
            if(Mathf.Abs(turnAmount) > 0)
            {
                if(forwardAmount >= 0)
                {
                    forwardAnim = 1;
                }
                else{
                    forwardAmount = -1;
                }
            }
            playerStates.anim.SetFloat("forward", forwardAnim, 0.2f, playerStates.deltaTime);

            return false;
        }
        public bool CameraOrientedControls()
        {
            Vector3 targetDirection = Vector3.zero;
            float forwardAnim = 0;
            if(playerStates.moveAmount > 0.1f){
                forwardAnim = 1;
                targetDirection = playerStates.moveDirection;
                Quaternion targetRot = Quaternion.LookRotation(playerStates.moveDirection);
                playerStates.mTransform.rotation = Quaternion.Slerp(playerStates.mTransform.rotation, targetRot, playerStates.deltaTime * playerStates.rotationSpeedCamOriented);
            }
            float forwardAmount = playerStates.moveAmount* playerStates.movementSpeed;
            Vector3 forward = playerStates.mTransform.forward * forwardAmount;// * playerStates.deltaTime;
            playerStates.agent.velocity = forward;
            //playerStates.mTransform.position += forward;
            playerStates.anim.SetFloat("forward", forwardAnim, 0.2f, playerStates.deltaTime);
            return false;
        }
    }
}
