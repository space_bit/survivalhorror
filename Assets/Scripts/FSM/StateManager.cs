﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space
{
    public abstract class StateManager : MonoBehaviour
    {
        State currentState;
        Dictionary<string, State> allStates = new Dictionary<string, State>();

        protected void RegisterState(string id, State state)
        {
            allStates.Add(id, state);
        }
        State GetState(string id)
        {
            allStates.TryGetValue(id, out State retVal);
            return retVal;
        }
        public void SetState(string id)
        {
            //State newState = GetState(id);
            currentState = GetState(id);
            if(currentState != null)
            {
                currentState.onEnter?.Invoke();
            }
        }
        private void Start()
        {
            Init();
        }
        protected abstract void Init();
        protected void Tick()
        {
            if (currentState != null)
            {
                currentState.Tick();
            }
        }
    }
}
