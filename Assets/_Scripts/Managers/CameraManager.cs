﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public class CameraManager : MonoBehaviour
    {
        public CameraHook currentCamera;
        public Cinemachine.CinemachineBrain cinemachineBrain;
        public void AssignActiveCamera(CameraHook newCamera)
        {
            cinemachineBrain.m_DefaultBlend = newCamera.onEnterBlendDefinition;
            if(currentCamera != null)
            {
                currentCamera.DisableCamera();
            }
            currentCamera = newCamera;
            currentCamera.EnableCamera();
        }
        public static CameraManager singleton;

        private void Awake()
        {
            singleton = this;   
        }

    }

}
