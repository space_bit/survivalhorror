﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Time Travel Interaction")]
    public class TimeTravelInteraction : Interaction
    {
        public override void Execute(PlayerLocomotion player, InteractionHook hook)
        {
            if(hook.interactionSteps[hook.stepIndex].gameObjectHook != null){
                player.StopInteraction();
                hook.interactionSteps[hook.stepIndex].gameObjectHook.SetActive(true);
                player.gameObject.SetActive(false);
                hook.gameObject.SetActive(false);
            }
            
        }
    }
}

