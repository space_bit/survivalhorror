﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public class InteractionHook : MonoBehaviour
    {
        public Interaction interaction;
        public bool isOneShot;
        public bool isAuto;
        public string interactionText = "Look";
        public int stepIndex;
        public InteractionStep[] interactionSteps;
        public GameObject gameObjectHook;
        public void ExecuteInteraction(PlayerLocomotion states, float delta)
        {
            if(interaction != null)
            {
                interaction.Execute(states, this);
            }
        
        }
        private void OnTriggerEnter(Collider other)
        {
            PlayerLocomotion playerStates = other.GetComponentInParent<PlayerLocomotion>();
            if (playerStates != null)
            {
                if(isAuto){
                    ReferenceManager.singleton.interactionTextObj.SetActive(true);
                    ReferenceManager.singleton.interactionText.text = interactionText;
                    interaction.Execute(playerStates, this);
                }else{
                    playerStates.LoadInteraction(this);
                }
                
            }
        }
        private void OnTriggerExit(Collider other)
        {

            PlayerLocomotion playerStates = other.GetComponentInParent<PlayerLocomotion>();
            if (playerStates != null)
            {
                playerStates.UnloadInteractionActual();
            }
        }
    }
}


