﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

namespace space
{
    public class PlayerStatesManager : StateManager
    {
        #region Variables
        public bool tankControls = true;
        [Header("Inputs")]
        public bool isAiming;
        public bool debugAnim;
        public bool isFiring;
        public bool isInteracting;
        public bool hasInteraction;
        public float horizontal;
        public float vertical;
        public float moveAmount;
        public Vector3 moveDirection;
        [HideInInspector]
        public float deltaTime;
        [Header("Controller Variables")]
        public float movementSpeed = 1.5f;
        public float rotationSpeed = 2f;
        public float rotationSpeedCamOriented = 2;
        [Header("References")]
        public Transform cameraTransform;
        public NavMeshAgent agent;
        public Animator anim;
        [HideInInspector]
        public Transform mTransform;
        //public InventoryManager inventoryManager;
        [SerializeField]
        //InteractionHook currentInteractionHook;
        public Text descriptionText; //make a UI handling thing pls
       /*  public InteractionHook GetInteractionHook{
            get{
                return currentInteractionHook;
            }
        }*/
        public GameObject interactionButton;
        #endregion

        #region State Ids
        string locomotionId = "locomotion";
        string aimingId = "aiming";
        string interactionsId = "interactions";
        #endregion
  
        protected override void Init(){
            mTransform = this.transform;
            InputManager inputManager = new InputManager(this);
            PlayerMovement playerMovement = new PlayerMovement(this, aimingId);

            State locomotion = new State(new List<StateAction>(){
                inputManager, 
                new DetectInteractions(this, interactionsId),
                playerMovement
            }
            );

            locomotion.onEnter = PlayLocomotionTree;

            State aiming = new State(new List<StateAction>(){
                inputManager,
                new Aiming(this,locomotionId, playerMovement)
            });

            State interacting = new State(new List<StateAction>(){
                inputManager,
                new HandleInteractions(this)
            });

            aiming.onEnter = PlayAimingAnimation;

            RegisterState(aimingId, aiming);
            RegisterState(locomotionId, locomotion);
            RegisterState(interactionsId, interacting);
            
            anim = GetComponentInChildren<Animator>();
            agent = GetComponentInChildren<NavMeshAgent>();
            agent.updateRotation  = false;

            //inventoryManager.LoadWeapon(this,inventoryManager.startingWeapon);

            //this is always last
            SetState(locomotionId);
        }

        void PlayAimingAnimation()
        {
            anim.CrossFade("Aiming", 0.05f);
        }
        void PlayLocomotionTree()
        {
            anim.CrossFade("Locomotion", 0.1f);
        }
        public void SetStateToLocomotion()
        {
            SetState(locomotionId);
        }
      /*  public void LoadInteraction(InteractionHook interactionHook)
        {
            hasInteraction = true;
            currentInteractionHook = interactionHook;
            interactionButton.SetActive(true);
        }*/ 
        public void UnloadInteraction()
        {
            interactionButton.SetActive(false);
            hasInteraction = false;
            //currentInteractionHook = null;
        }
        private void Update()
        {
            deltaTime = Time.deltaTime;
            Tick();
        }
    }
}