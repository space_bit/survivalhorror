﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace space2
{
    [System.Serializable]
    public class InteractionStep 
    {   
        public string stepText;
        public Interaction callbackInteraction;
        public GameObject gameObjectHook;

    }
}

