using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public abstract class State : MonoBehaviour
    {
        public abstract void Tick (float delta);
        
    }
}
