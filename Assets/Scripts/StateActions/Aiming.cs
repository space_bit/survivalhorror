using UnityEngine;
using System.Collections;

namespace space
{
    public class Aiming : StateAction
    {
        PlayerStatesManager playerStates;
        string locomotionId;
        PlayerMovement playerMovement;
        
        public Aiming ( PlayerStatesManager playerStatesManager, string locomotionId, PlayerMovement pm){
            playerMovement = pm;
            this.locomotionId = locomotionId;
            playerStates = playerStatesManager;
        }
        public override bool Execute()
        {
            bool retVal = false;
            if(playerStates.isFiring)
            {
                //HandleShooting();
            }
            else
            {
                playerMovement.TankControls();
            }
            
            if(playerStates.isAiming == false)
            {
                playerStates.SetState(locomotionId);
                retVal = true;
            }
            return retVal;
        }

        float lastFired;
        /*void HandleShooting()
        {
            Weapon cw = playerStates.inventoryManager.GetCurrentWeapon();
            if(cw != null)
            {
                if(cw.fireRate < Time.realtimeSinceStartup - lastFired)
                {
                    lastFired = Time.realtimeSinceStartup;

                    Vector3 origin = playerStates.mTransform.position+Vector3.up;
                    Vector3 direction = playerStates.mTransform.forward;

                    Debug.DrawRay(origin, direction * 20);
                    RaycastHit hit;
                    if(Physics.Raycast(origin, direction, out hit, 20))
                    {
                        Debug.Log(hit.transform.name);
                        IShootable shootable = hit.transform.GetComponent<IShootable>();
                        if(shootable != null)
                        {
                            shootable.OnHit(cw);
                        }
                    }
                }
            }
        }*/
    }
}