﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Quit")]
    public class Quit : Interaction
    {
        public bool interrupt;
        public override void Execute(PlayerLocomotion player, InteractionHook hook)
        {
            
            Application.Quit();
        }
    }
}

