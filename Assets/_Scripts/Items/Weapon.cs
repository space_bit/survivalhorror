using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Items/Weapon")]
    public class Weapon : Item
    {
        public GameObject modelPrefab;

        public AnimatorOverrideController animOverrideController;
        //icon
        //stats

        public bool isBurstFire;
        public float fireRate = .4f;
        public int magAmount = 12;
        public int baseDamage = 5;

        [System.NonSerialized]
        public GameObject runtimeModel;
        [System.NonSerialized]
        public WeaponHook weaponHook;
    }
}

