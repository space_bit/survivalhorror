﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace space2
{
    [RequireComponent(typeof(EnemyLocomotion))]
    public class EnemyAttacking : State
    {
        EnemyLocomotion locomotionState;
        StateManager stateManager;
        private void Start()
        {
            locomotionState = GetComponent<EnemyLocomotion>();
            stateManager = GetComponent<StateManager>();
        }
        public override void Tick(float delta)
        {
            bool isInteracting = locomotionState.anim.GetBool("isInteracting");
            locomotionState.agent.velocity = locomotionState.animHook.rootMotionDirection;

            if(!isInteracting)
            {
                locomotionState.anim.applyRootMotion = false;
                locomotionState.agent.isStopped = false;
                stateManager.ChangeState(locomotionState);
            }
        }
    }

}
