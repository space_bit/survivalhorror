using UnityEngine;
using System.Collections;

namespace space
{
    public class InputManager : StateAction
    {
        PlayerStatesManager playerStates;
        Vector3 scaleVector = new Vector3 (1,0,1);
        public InputManager(PlayerStatesManager playerStatesManager)
        {
            playerStates = playerStatesManager;
        }
        public override bool Execute()
        {
            playerStates.isAiming = Input.GetButton("Fire2");
            playerStates.isFiring = Input.GetButton("Fire1");
            playerStates.isInteracting = Input.GetKeyDown(KeyCode.E);
            playerStates.horizontal = Input.GetAxis("Horizontal");
            playerStates.vertical = Input.GetAxis("Vertical");
            playerStates.moveAmount = Mathf.Clamp01(Mathf.Abs(playerStates.horizontal) + Mathf.Abs(playerStates.vertical));

            Vector3 camForward = Vector3.Scale(playerStates.cameraTransform.forward, scaleVector).normalized;
            playerStates.moveDirection = (playerStates.vertical * camForward) + (playerStates.horizontal * playerStates.cameraTransform.right);
            playerStates.moveDirection.Normalize();
            if(playerStates.debugAnim)
            playerStates.isAiming = true;
            return false;
        }
    }
}