using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public  class AnimatorHook : MonoBehaviour
    {
        public Animator anim;
        public Vector3 rootMotionDirection;
        private void Start()
        {
            anim = GetComponent<Animator>();

        }

        public void HandleRootMotion(bool status)
        {
            anim.applyRootMotion = status;
        }
        private void OnAnimatorMove(){
            rootMotionDirection = anim.deltaPosition / Time.deltaTime;
        }
    }
}
