﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProximity : MonoBehaviour
{
    public Transform playerObj;
    public AnimationCurve ditherCurve;
    Material DitherMat = null;
    float distance;
    float value;
    float speed;

    // Start is called before the first frame update
    void Start()
    {
        DitherMat = GetComponentInChildren<Renderer>().material;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        distance = Vector3.Distance(this.transform.position, playerObj.position);
        value = ditherCurve.Evaluate(distance);
        DitherMat.SetFloat("_Distance", value);

        Vector3 targetDir = playerObj.position - transform.position;

        // The step size is equal to speed times frame time.
        float step = speed * Time.deltaTime;

        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0f);
        Debug.DrawRay(transform.position, newDir, Color.red);

        // Move our position a step closer to the target.
        transform.rotation = Quaternion.LookRotation(newDir);
    }
}
