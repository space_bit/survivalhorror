using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace space2
{
    public class CameraHook : MonoBehaviour
    {
        public GameObject cm_obj;
        public CameraHook onExitEnableHook;
        public bool onStartAssign;
        public CinemachineBlendDefinition onEnterBlendDefinition;
        private void Start()
        {
            cm_obj.SetActive(false);
            if (onStartAssign)
            {
                CameraManager.singleton.AssignActiveCamera(this);
            }
        }

        public void EnableCamera()
        {
            cm_obj.SetActive(true);
        }

        public void DisableCamera()
        {
            cm_obj.SetActive(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            PlayerLocomotion player = other.GetComponentInParent<PlayerLocomotion>();
            if (player != null)
            {
                CameraManager.singleton.AssignActiveCamera(this);
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (onExitEnableHook == null)
                return;
            PlayerLocomotion playerStates = other.GetComponentInParent<PlayerLocomotion>();
            if (playerStates != null)
            {
                CameraManager.singleton.AssignActiveCamera(onExitEnableHook);
            }
        }

    }

}
