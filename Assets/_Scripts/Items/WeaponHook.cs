using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public class WeaponHook : MonoBehaviour
    {
        public int bulletsInMag = 12;
        int defaultMag;
        float fireRateTimer;
        ParticleSystem[] particles;
        public AudioSource gunsound;

        public void Init(Weapon weapon)
        {
            particles = GetComponentsInChildren<ParticleSystem>();
            defaultMag = weapon.magAmount;
        }

        public void ShootWeapon()
        {
            for(int i = 0; i < particles.Length; i++)
            {
                particles[i].Play();
            }
            if(gunsound != null)
            {
                gunsound.Play();
            }
            //Play particles
            //Play sound
        }
    }
}

