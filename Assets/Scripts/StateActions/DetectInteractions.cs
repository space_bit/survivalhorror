using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space
{
    public class DetectInteractions : StateAction
    {
        PlayerStatesManager states;
        string interactionsState;
        
        public DetectInteractions(PlayerStatesManager states, string interactionsId)
        {
            this.states = states;
            interactionsState = interactionsId;
        }
        public override bool Execute()
        {
            bool retVal  = false;
            if(states.hasInteraction)
            {
                if(states.isInteracting)
                {
                    //change state
                    states.SetState(interactionsState);
                    retVal = true;
                }
            }
            return retVal;
        }
    }
}


