using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public interface IShootable
    {
        void OnHit(Weapon weapon);
    }
}