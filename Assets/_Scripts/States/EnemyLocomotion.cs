﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace space2
{
    public class EnemyLocomotion : State, IShootable
    {
        [Header("GameStats")]
        public int health;
        public int crawlChance = 70;
        [Header("States")]
        public bool hasPlayer;
        public bool isMoving;
        public bool isInteracting;
        [Header("References")]
        public Animator anim;
        public AnimatorHook animHook;
        public NavMeshAgent agent;

        [Header("ControllerStats")]
        public float detectDistance = 4;
        public float movementSpeed = 2;
        public float rotationSpeed = 15;
        [Header("AI Stats")]
        public float attackDistance = 1;
        public string attackAnim = "Bite";

        Transform mTransform;

        [Header("Debug Values")]
        public Transform playerTransform;
        EnemyAttacking enemyAttackingState;
        StateManager stateManager;
        private void Start()
        {
            mTransform = this.transform;
            agent.speed = movementSpeed;
            agent.angularSpeed = rotationSpeed;
            enemyAttackingState = GetComponent<EnemyAttacking>();
            stateManager = GetComponent<StateManager>();

        }
        public override void Tick(float delta)
        {
            if (isInteracting)
            {
                agent.speed = 0;
                agent.angularSpeed = 0;
                isInteracting = anim.GetBool("isInteracting");
                return;
            }
            if (!hasPlayer)
            {
                PlayerDetection();
            }
            else
            {
                MoveToPlayer(delta);
            }
        }
        void PlayerDetection()
        {
            //print(mTransform);
            //print(playerTransform);
            float distance = Vector3.Distance(mTransform.position, playerTransform.position);

            if (distance < detectDistance)
            {
                hasPlayer = true;
            }
        }
        void MoveToPlayer(float delta)
        {
            float distance = Vector3.Distance(mTransform.position, playerTransform.position);
            if (distance <= attackDistance)
            {
                //agent.speed = 0;
                //agent.angularSpeed = 0;
                //agent.isStopped = true;
                anim.applyRootMotion = true;
                anim.SetBool("isInteracting", true);
                anim.CrossFade(attackAnim, 0.2f);
                stateManager.ChangeState(enemyAttackingState);
                
            }
            else
            {
                agent.SetDestination(playerTransform.position);
                anim.SetFloat("forward", 1, 0.2f, delta);

                Vector3 dir = animHook.rootMotionDirection;
                Vector3 reldir = mTransform.InverseTransformDirection(dir);
                agent.speed = reldir.z * movementSpeed;
                agent.angularSpeed = rotationSpeed;
            }


        }
        public void OnHit(Weapon weapon)
        {
            int d = weapon.baseDamage;
            health -= d;
            if (health < 0)
            {
                int ran = Random.Range(0, 101);
                if (ran < crawlChance)
                {
                    anim.CrossFade("death2", 0.2f);
                    anim.SetBool("isInteracting", true);
                    anim.SetBool("isCrawling", true);

                }
                else
                {
                    anim.CrossFade("death1", 0.2f);
                }
            }
        }
    }
}

