﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space
{
    public class State
    {
        List<StateAction> updateActions;
        bool forceExit;
        public delegate void OnEnter();
        public OnEnter onEnter;

        public State(List<StateAction> updateActions)
        {
            this.updateActions = updateActions;
        }

        public void Tick()
        {
            forceExit = false;
            ExecuteActionsList(updateActions);
        }

        void ExecuteActionsList(List<StateAction> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                if (forceExit)
                {
                    return;
                }
                forceExit = l[i].Execute();
            }
        }
    }
}