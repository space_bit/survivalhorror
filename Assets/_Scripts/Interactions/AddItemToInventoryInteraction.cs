﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Add Item To Inventory Interaction")]
    public class AddItemToInventoryInteraction : Interaction
    {
        public string itemId;
        public override void Execute(PlayerLocomotion player, InteractionHook hook){
            Debug.Log("Item Added");
        }
    }
}

