﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Open GO on Hook Interaction")]
    public class OpenGOonHook_Interaction : Interaction
    {
        public bool interrupt;
        public override void Execute(PlayerLocomotion player, InteractionHook hook)
        {
            if (hook.gameObjectHook != null)
                hook.gameObjectHook.SetActive(true);

            if (interrupt)
            {
                player.StopInteraction();
                if (hook.isOneShot)
                {
                    hook.gameObject.SetActive(false);
                }


            }
            if (hook.interactionSteps[hook.stepIndex].gameObjectHook != null)
            {
                hook.interactionSteps[hook.stepIndex].gameObjectHook.SetActive(true);
            }
            if (interrupt)
            {
                player.StopInteraction();
                if (hook.isOneShot)
                {
                    hook.interactionSteps[hook.stepIndex].gameObjectHook.SetActive(false);
                }
            }

        }
    }
}

