﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    public class StateManager : MonoBehaviour
    {
        [SerializeField]
        State currentState;
        [SerializeField]
        State defaultState;
        // Start is called before the first frame update
        void Start()
        {
            BackToDefaultState();
        }

        // Update is called once per frame
        void Update()
        {
            if(currentState != null)
            {
                currentState.Tick(Time.deltaTime);
            }
        }

        public void ChangeState(State targetState)
        {
            if(currentState != null)
            {
                //on exit
            }
            currentState = targetState;
            if(currentState != null)
            {
                //on enter
            }
        }
        public void BackToDefaultState()
        {
            ChangeState(defaultState);
        }
    }
}
