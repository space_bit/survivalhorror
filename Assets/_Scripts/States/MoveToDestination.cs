﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class MoveToDestination : MonoBehaviour
{
    public Transform target;
    public Animator anim;
    public bool hideOnArrival = false;
    Vector3 destination;
    NavMeshAgent agent;

    void Start()
    {
        // Cache agent component and destination
        agent = GetComponent<NavMeshAgent>();
        //destination = agent.destination;
    }

    void Update()
    {
        // Update destination if the target moves one unit
        if (Vector3.Distance(destination, target.position) > 1.0f)
        {
            destination = target.position;
            agent.destination = destination;
            //agent.speed = 1;
            anim.SetFloat("forward", 1, 0.2f, Time.deltaTime * 25);
        }
        if (Vector3.Distance(this.transform.position, target.position) <= agent.stoppingDistance)
        {
            anim.SetFloat("forward", 1, 0.2f, Time.deltaTime * 25);
            if(hideOnArrival)
            {
                this.gameObject.SetActive(false);
            }
        }

    }

}