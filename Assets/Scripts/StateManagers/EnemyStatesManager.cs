using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace space
{
    public class EnemyStatesManager : StateManager
    {
        public Animator anim;
        public int health = 10;
        public NavMeshAgent agent;
        /*public void OnHit(Weapon weapon)
        {
            health -= weapon.baseDamage;
            if(health < 0)
            {
                KillEnemy();
            }
        }*/

        protected override void Init()
        {
            anim = GetComponentInChildren<Animator>();
            agent = GetComponent<NavMeshAgent>();
        }
        public void KillEnemy()
        {
            agent.enabled = false;
            anim.applyRootMotion = true;
            anim.Play("death1");
        }
    }
}