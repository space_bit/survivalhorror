using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [System.Serializable]
    public class InventoryManager
    {
        public Weapon startingWeapon;
        Weapon currentWeapon;
        public Weapon GetCurrentWeapon()
        {
            return currentWeapon;
        }
        public AnimatorOverrideController defaultController;
        public void LoadWeapon(PlayerLocomotion playerLocomotion, Weapon targetWeapon)
        {
            if (currentWeapon != null)
            {
                if (currentWeapon.runtimeModel != null)
                {
                    currentWeapon.runtimeModel.SetActive(false);
                }
                currentWeapon = null;
            }

            if (targetWeapon == null)
            {
                playerLocomotion.anim.runtimeAnimatorController = defaultController;
                return;
            }

            Transform rightHand = playerLocomotion.anim.GetBoneTransform(HumanBodyBones.RightHand);
            if (targetWeapon.runtimeModel == null)
            {
                
                targetWeapon.runtimeModel = GameObject.Instantiate(targetWeapon.modelPrefab) as GameObject;
                targetWeapon.runtimeModel.transform.SetParent(rightHand);
                targetWeapon.runtimeModel.transform.localPosition = Vector3.zero;
                targetWeapon.runtimeModel.transform.localRotation = Quaternion.identity;
                targetWeapon.runtimeModel.transform.localScale = Vector3.one;
                targetWeapon.weaponHook = targetWeapon.runtimeModel.GetComponentInChildren<WeaponHook>();
                targetWeapon.weaponHook.Init(targetWeapon);
            }
            else
            {
                targetWeapon.runtimeModel.SetActive(true);
            }
            playerLocomotion.anim.runtimeAnimatorController = targetWeapon.animOverrideController;
            currentWeapon = targetWeapon;
        }

    }
}

