﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Close GO on Hook Interaction")]
    public class CloseGOonHook_Interaction : Interaction
    {
        public bool interrupt;
        public override void Execute(PlayerLocomotion player, InteractionHook hook)
        {
            if(hook.interactionSteps[hook.stepIndex].gameObjectHook != null){
                hook.interactionSteps[hook.stepIndex].gameObjectHook.SetActive(false);
            }
            if(interrupt){
                player.StopInteraction();
                if(hook.isOneShot)
                {
                    hook.interactionSteps[hook.stepIndex].gameObjectHook.SetActive(true);
                }
            }
            
        }
    }
}

