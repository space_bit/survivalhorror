using UnityEngine;
using System.Collections;

namespace space2
{
    [CreateAssetMenu(menuName = "Interactions/Look Interaction")]
    public class LookInteraction : Interaction
    {
        public bool freezeTime;

        bool isInit;
        public string animationName;

        public override void Execute(PlayerLocomotion player, InteractionHook hook)
        {
            if (freezeTime)
                Time.timeScale = 0;

            if (!isInit)
            {
                isInit = true;
                if (!string.IsNullOrEmpty(animationName))
                    player.anim.Play(animationName);

                player.anim.SetFloat("forward", 0);
            }

            if (player.aimingInput || player.fireInput || player.interactInput)
            {
                hook.stepIndex++;

                if (hook.stepIndex > hook.interactionSteps.Length - 1)
                {
                    hook.stepIndex = 0;

                    if (hook.isOneShot)
                    {
                        hook.gameObject.SetActive(false);
                    }

                    ReferenceManager.singleton.interactionTextObj.SetActive(false);
                    player.StopInteraction();

                    if (freezeTime)
                        Time.timeScale = 1;

                    isInit = false;
                    return;
                }

            }

            ReferenceManager.singleton.interactionTextObj.SetActive(true);
            ReferenceManager.singleton.interactionText.text = hook.interactionSteps[hook.stepIndex].stepText;
            if (hook.interactionSteps[hook.stepIndex].callbackInteraction != null)
            {
                hook.interactionSteps[hook.stepIndex].callbackInteraction.Execute(player, hook);
                /*                
				for (int i = 0; i < hook.interactionSteps[hook.stepIndex].callbackInteraction.Length; i++)
                {
                    hook.interactionSteps[hook.stepIndex].callbackInteraction[i].Execute(player, hook);
                } */
            }

        }
    }
}
